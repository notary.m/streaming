import * as React from 'react';

import { Header } from '../Header';
import { Search } from '../Search';
import { Video } from '../Video';

import { Switch, Route, Router } from 'react-router';
import { History } from 'history';

import './Application.scss';

export interface IProps {
    history: History
}

export class Application extends React.PureComponent<IProps, {}> {
    public render() {
        return (
            <Router history={this.props.history}>
                <div>
                    <Header />
                    <main>
                        <Switch>
                            <Route exact path="/" component={Search} />
                            <Route exact path="/video/:videoId" component={Video} />
                        </Switch>
                    </main>
                </div>
            </Router>
        );
    }
}
