import * as React from 'react';
import { IChat, createChatReader, ChatReader } from '../../services/livechat';

import './Chat.scss';
import { Messages } from '../Messages';

export interface IState {
    chat?: IChat;
}

export interface IProps {
    liveChatId: string;
}

export class Chat extends React.PureComponent<IProps, IState> {
    state: IState = {};
    chatReader: ChatReader | null = null;

    constructor(props: IProps) {
        super(props);

        this.handlePollingMessages = this.handlePollingMessages.bind(this);
    }

    public render() {
        return (
            <div className="chat">
                {this.getMessages()}
            </div>
        );
    }

    private getMessages() {
        if (!this.state.chat) {
            return null;
        }

        return <Messages items={this.state.chat.items} />;
    }

    public componentDidMount() {
        this.chatReader = createChatReader({
            liveChatId: this.props.liveChatId,
            pollingInterval: 0,
            items: []
        }, this.handlePollingMessages);

        this.chatReader.start();
    }

    componentWillUnmount() {
        if (this.chatReader) {
            this.chatReader.stop();
        }
    }

    protected handlePollingMessages(chat: IChat | null) {
        if (!chat) return;

        this.setState({
            chat
        });
    }
}
