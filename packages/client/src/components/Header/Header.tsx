import * as React from 'react';
import { Link } from 'react-router-dom';
import { user, IUser } from '../../services/user';
import { auth } from '../../services/auth';

import './Header.scss';

interface IState {
    user?: IUser;
    needAuth?: boolean;
}

export class Header extends React.Component<{}, IState> {
    state: IState = {};

    componentDidMount() {
        user()
            .then(user => this.setState({ user }))
            .catch(() => this.setState({ needAuth: true }));
    }

    public render() {
        const user = this.renderUser();

        return (
            <header className="header">
                <Link className="link header-link" to="/"><div className="logo">Streaming</div></Link>
                {user}
            </header>
        );
    }

    private renderUser() {
        const user = this.state.user;
        if (user) {
            return (
                <div className="user">
                    <img className="user-avatar" src={user.imageUrl} alt="avatar" />
                    <span className="user-name">{user.name}</span>
                </div>
            );
        }

        if (this.state.needAuth) {
            return (
                <div className="user">
                    <a className="link header-link" href={auth()} target="_self"><span>Sing In with Youtube</span></a>
                </div>
            );
        }
    }
}
