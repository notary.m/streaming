import * as React from 'react';
import { IChatMessage } from "../../services/livechat";

import './Messages.scss';

export interface IProps {
    items?: IChatMessage[];
    className?: string;
}

export function Messages(props: IProps): React.ReactElement<IProps> | null {
    if (!props.items) return null;

    const className = props.className;

    return (
        <div className={`messages${className ? ' ' + className : ''}`}>
            {
                props.items.reverse().map((item, index) => (
                    <div className="message" key={index}>
                        <div className="message-details">
                            <span className="message-author">{item.author}</span>
                            <span className="message-time">{(new Date(item.message.publishedAt)).toLocaleTimeString()}</span>
                        </div>
                        <div className="message-text">
                            {item.message.text}
                        </div>
                    </div>
                ))
            }
        </div>
    );
}
