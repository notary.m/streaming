import * as React from 'react';
import { search, ISearchItem } from '../../services/youtube';
import { Link, RouteComponentProps } from 'react-router-dom';
import { SearchInput } from '../SearchInput';
import { parse, stringify } from 'query-string';

import './Search.scss';

export interface IState {
    text: string;
    items?: ISearchItem[];
}

export class Search extends React.Component<RouteComponentProps<{}>, IState> {
    public state: IState = { text: '' };

    public render() {
        return (
            <div className="search">
                <SearchInput text={this.state.text} onSearchClick={this.handleSearchClick.bind(this)} />
                {
                    this.renderItemsList()
                }
            </div>
        );
    }

    public componentDidMount() {
        const search = parse(this.props.location.search);

        if (search.text) {
            this.search(this.state.text);
        }
    }

    private renderItemsList() {
        if (!this.state.items) return null;

        return (
            <ul className="search-items">
                {this.state.items.map(item => this.renderItem(item), this)}
            </ul>
        );
    }

    private renderItem(item: ISearchItem) {
        return (
            <li
                className="item"
                key={item.videoId}
            >
                <Link className="link" to={`/video/${item.videoId}`}>
                    <img
                        className="item-thumb"
                        src={item.thumb.url}
                        height={item.thumb.h / 2}
                        alt="thumb"
                    />
                    <div className="item-snippet">
                        <h2 className="item-title">{item.title}</h2>
                        <div className="item-description">{item.description}</div>
                    </div>
                </Link>
            </li>
        );
    }

    protected handleSearchClick(text: string) {
        this.props.history.push({
            pathname: this.props.location.pathname,
            search: stringify({ ...parse(this.props.location.search), text })
        });

        this.search(text);
    }

    protected search(text: string) {
        search(text).then(
            items => this.setState({ items })
        );
    }

    static getDerivedStateFromProps(nextProps: RouteComponentProps<{}>, prevState: IState) {
        const search = parse(nextProps.location.search);

        if (prevState.text || !search.text) {
            return prevState;
        }

        return {
            ...prevState,
            text: search.text
        }
    }
}
