import * as React from 'react';

import './SearchInput.scss';

export interface IState {
    text: string;
    isTextChanged: boolean;
}

export interface IProps {
    text?: string;
    onChange?: (text: string) => void;
    onSearchClick: (text: string) => void;
}

export class SearchInput extends React.PureComponent<IProps, IState> {
    state: IState = { text: '', isTextChanged: false };

    public render() {
        return (
            <>
                <input className="input" type="text" value={this.state.text} onChange={e => this.handleChange(e)} />
                <button className="search-button" onClick={() => this.props.onSearchClick(this.state.text)}>Search</button>
            </>
        );
    }

    protected handleChange(e: any) {
        const text = e.target.value;
        this.setState({ text, isTextChanged: true })

        if (this.props.onChange) {
            this.props.onChange(text);
        }
    };


    static getDerivedStateFromProps(nextProps: IProps, prevState: IState) {
        if (nextProps.text &&
            typeof prevState.text !== 'string' ||
            !prevState.isTextChanged
        ) {
            return {
                ...prevState,
                text: nextProps.text
            };
        }

        return prevState;
    }
}
