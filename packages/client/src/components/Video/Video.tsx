import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { video, IVideo } from '../../services/youtube';
import { Chat } from '../Chat';
import { SearchInput } from '../SearchInput';
import { getMessagesFromChat, IChatMessage } from '../../services/livechat';

import './Video.scss';
import { Messages } from '../Messages';

const PLAYER_SRC = 'https://www.youtube.com/embed';

export class Video extends React.Component<RouteComponentProps<IVideo>, IVideo & { messagesFromAuthor: IChatMessage[] }> {
    constructor(props: RouteComponentProps<IVideo>) {
        super(props);

        this.handleSearchClick = this.handleSearchClick.bind(this);
    }

    public render() {
        const videoId = this.state.videoId;

        return (
            <>
                {this.renderTitle()}
                <div className="layout">
                    <iframe
                        className="layout-video"
                        id="player"
                        width="640"
                        height="360"
                        allowFullScreen
                        src={`${PLAYER_SRC}/${videoId}?autoplay=1&html5=1&fs=1&origin=${this.getOrigin()}`}
                        frameBorder="0"
                    />
                    <div className="layout-chat">
                        {
                            this.state.liveChatId ? (
                                <Chat liveChatId={this.state.liveChatId} />
                            ) : null
                        }
                    </div>
                </div>
                <div className="filter">
                    <SearchInput text={''} onSearchClick={this.handleSearchClick} />
                    <Messages className="filter-messages" items={this.state.messagesFromAuthor} />
                </div>
            </>
        );
    }

    private renderTitle() {
        const details = this.state.videoDetails;

        if (!details) return null;

        return (
            <h2 className="title">{details.snippet.title}</h2>
        );
    }

    public componentDidMount() {
        video(this.state.videoId)
            .then(videoDetails => {
                const details = videoDetails.items[0];
                const liveChatId = details.liveStreamingDetails.activeLiveChatId
                this.setState({
                    liveChatId,
                    videoDetails: details
                });
            });
    }

    protected getOrigin() {
        return `${location.protocol}//${location.host}`;
    }

    private handleSearchClick(text: string) {
        if (!this.state.liveChatId) return;

        const messages = getMessagesFromChat(this.state.liveChatId);

        if (!messages || !messages.length) return;

        const messagesFromAuthor = messages.filter(
            message => message.author.toLowerCase() === text.toLowerCase()
        );

        this.setState({ messagesFromAuthor });
    }

    static getDerivedStateFromProps(nextProps: RouteComponentProps<IVideo>, prevState: IVideo) {
        const routeMatch = nextProps.match;
        const videoId = routeMatch.params.videoId;

        return {
            ...prevState,
            videoId
        }
    }
}
