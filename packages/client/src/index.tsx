import * as React from 'react';
import { render } from 'react-dom';
import { createBrowserHistory } from 'history';

import { Application } from './components/Application';

document.addEventListener('DOMContentLoaded', () => {
    const root = document.getElementById('app');

    if (!root) {
        return;
    }

    function renderApp() {
        render(<Application history={createBrowserHistory({ basename: '/' })}/>, root);
    }

    renderApp();

    if ((module as any).hot) (module as any).hot.accept('./components/Application', () => renderApp());
});
