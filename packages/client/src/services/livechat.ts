import { config } from "./api.config";

export interface IChatMessage {
    author: string;
    message: {
        text: string;
        publishedAt: Date;
    }
}

export interface IChat {
    liveChatId: string;
    pollingInterval: number;
    items: IChatMessage[]
}

export type ChatCallback = (chat: IChat | null) => void;

const chatMap = new Map<string, IChatMessage[]>();

export class ChatReader {
    private endpoint: string;
    private timer: NodeJS.Timer | null = null;

    constructor(
        private chat: IChat,
        private callback: ChatCallback) {
        this.endpoint = `${config.HOST}/api/livechat/messages?liveChatId=${this.chat.liveChatId}`;
    }

    public start() {
        this.timer = setTimeout(() => {
            fetch(this.endpoint, {
                credentials: "same-origin"
            })
                .then(result => result.json())
                .then(chatResult => {
                    chatResult.liveChatId = this.chat.liveChatId;
                    this.chat = chatResult;

                    chatMap.set(this.chat.liveChatId, this.chat.items);

                    return this.chat;
                })
                .then(chatResult => this.callback(chatResult))
                .then(() => this.start())
                .catch(() => this.callback(null));
        }, this.chat.pollingInterval);
    }

    public stop() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }
    }
}

export function createChatReader(chat: IChat, callback: ChatCallback) {
    return new ChatReader(chat, callback);
}

export function getMessagesFromChat(liveChatId: string) {
    return chatMap.get(liveChatId);
}
