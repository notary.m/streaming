export interface IStoredItem<T = any> {
    timestamp: number;
    data: T
};

export type Store = Record<string | number, IStoredItem>;

const store: Store = {};
const tenMinute = 60000 * 10;

export function hasData(param: string | number): boolean {
    const record = store[param];

    if (!record) return false;
    if (Date.now() - record.timestamp > tenMinute) return false;

    return true;
}

export function getData<T>(param: string | number): T | null {
    if (!hasData(param)) return  null;

    return store[param].data as T;
}

export function putData(param: string | number, item: IStoredItem): void {
    store[param] = {
        timestamp: Date.now(),
        data: item
    };
}
