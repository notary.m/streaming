import { config } from "./api.config";

export interface IUser {
    name: string;
    id: string;
    imageUrl: string;
}

export function user(): Promise<IUser> {
    const endpoint = `${config.HOST}/api/user`;

    return new Promise<IUser>((resolve, reject) => {
        fetch(endpoint, {
            credentials: "same-origin"
        })
        .then(result => result.json())
        .then(rawUser => {
            resolve({
                name: rawUser.displayName,
                id: rawUser.id,
                imageUrl: rawUser.image.url
            });
        })
        .catch(e => reject(e));
    });
}
