import { hasData, getData, putData } from './store';
import { config } from './api.config';

export interface ISearchItem {
    videoId: string;
    channelId: string;
    title: string;
    description: string;
    thumb: {
        url: string;
        h: number;
        w: number;
    }
}

export interface IVideoItem {
    liveStreamingDetails: {
        activeLiveChatId: string;
    },

    snippet: {
        title: string;
    }
}

export interface IVideo {
    videoId: string;
    videoDetails?: IVideoItem;
    items: [IVideoItem];
    liveChatId?: string;
}

export function search(text: string): Promise<ISearchItem[]> {
    if (hasData(text)) {
        const result = getData<ISearchItem[]>(text);

        if (result) {
            return Promise.resolve(result);
        }
    }

    const endpoint = `${config.HOST}/api/search?text=${text}`;

    return new Promise<ISearchItem[]>((resolve, reject) => {
        fetch(endpoint, {
            credentials: "same-origin"
        })
            .then(result => result.json())
            .then(rawSearchResult => {
                putData(text, rawSearchResult);
                resolve(rawSearchResult)
            })
            .catch(() => reject());
    });
}

export function video(videoId: string): Promise<IVideo> {
    if (hasData(videoId)) {
        const result = getData<IVideo>(videoId);

        if (result) {
            return Promise.resolve(result);
        }
    }

    const endpoint = `${config.HOST}/api/video?videoId=${videoId}`;

    return new Promise<IVideo>((resolve, reject) => {
        fetch(endpoint, {
            credentials: "same-origin"
        })
            .then(result => result.json())
            .then(rawVideoResult => {
                putData(videoId, rawVideoResult);
                resolve(rawVideoResult)
            })
            .catch(() => reject());
    });
}
