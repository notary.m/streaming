const { resolve } = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const isDevelopment = process.env.NODE_ENV === 'development';
const publicPath = '/public/';

const entryList = [
    resolve('src', 'index.tsx')
];

module.exports = {
    mode: process.env.NODE_ENV,
    name: 'client',
    entry: {
        core: entryList
    },
    output: {
        path: resolve('public'),
        filename: `[name].js`,
        publicPath
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.scss', '.css', '.svg', '.html', '.js', '.jsx'],
        modules: [
            'node_modules'
        ]
    },
    module: {
        rules: [
            {
                test: /.tsx?$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ['env', {
                                    targets: {
                                        browsers: ['last 2 versions', 'safari >= 7']
                                    }
                                }],
                                'stage-2',
                                'react'
                            ],
                            plugins: [
                                'transform-react-remove-prop-types',
                                'transform-react-constant-elements',
                                'transform-object-rest-spread',
                                'transform-object-assign'
                            ]
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    { loader: 'postcss-loader' }
                ]
            },
            {
                test: /\.(png|jpg|gif|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 1000,
                    name: '[name].[ext]'
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-url-loader',
                options: {
                    stripdeclarations: true,
                    encoding: 'none'
                }
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            'process.env.HOST': JSON.stringify(isDevelopment ? 'https://p.streaming.mvictorov.com' : 'https://streaming.mvictorov.com')
        }),
        new MiniCssExtractPlugin({
            filename: 'main.css'
        }),
        new TsconfigPathsPlugin({
            configFile: resolve('./tsconfig.json')
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        isDevelopment ? new webpack.NamedModulesPlugin() : () => { },
        isDevelopment ? new webpack.NoEmitOnErrorsPlugin() : () => { },
        process.env.BUNDLE_ANALYSIS ? new BundleAnalyzerPlugin() : () => { }
    ],

    optimization: {
        splitChunks: {
            name: 'main',
            minChunks: Infinity
        },
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        beautify: false,
                        comments: false,
                    },
                    compress: {
                        drop_console: true,
                        sequences: true,
                        properties: true,
                        dead_code: true,
                        drop_debugger: true,
                        conditionals: true,
                        comparisons: true,
                        evaluate: true,
                        booleans: true,
                        loops: true,
                        unused: true,
                        if_return: true,
                        join_vars: true,
                        negate_iife: true,
                        hoist_funs: true,
                        warnings: false
                    }
                }
            }),
            new OptimizeCSSAssetsPlugin({})
        ]
    },

    devtool: isDevelopment ? 'inline-source-map' : false,
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
    stats: 'normal',
    devServer: {
        compress: true,
        clientLogLevel: 'none',
        contentBase: resolve('./public'),
        watchContentBase: true,
        historyApiFallback: true,
        hot: true,
        publicPath: publicPath,
        quiet: true,
        watchOptions: {
            ignored: /node_modules/
        },
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        },
        https: true,
        port: 3001,
        overlay: false,
        proxy: {
            '/api': 'https://p.streaming.mvictorov.com'
        }
    }
};
