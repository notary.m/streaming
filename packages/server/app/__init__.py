from flask import Flask
from flask_cors import CORS

app = Flask(__name__)

app.secret_key = '452414Piqwery//test/key'

CORS(app, resources={r"/api/*": {"origins": "*"}})

from app.controllers import user
from app.controllers import auth
