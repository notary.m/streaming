import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

from flask_cors import cross_origin

from app import app

import os
import flask
import requests

from flask import request


CLIENT_SECRETS_FILE = "client_secret.json"

SCOPES = ['https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.me']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

@app.route('/register')
def register():
    flow = get_flow()

    authorization_url, state = flow.authorization_url(
        access_type='offline',
        include_granted_scopes='true')

    return flask.redirect(authorization_url)

@app.route('/oauth2/callback')
def auth():
    flow = get_flow()

    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)
    flask.session['credentials'] = credentials_to_dict(flow.credentials)

    return flask.redirect('https://p.streaming.mvictorov.com/', code=302)

@app.route('/video')
@cross_origin()
def video():
    youtube = get_youtube_authenticated_service()

    video_id = request.args.get('videoId')

    response = youtube.videos().list(
        id=video_id,
        part='id,snippet,contentDetails,liveStreamingDetails,statistics,player',
    ).execute()

    return flask.jsonify(response)

@app.route('/livechat/messages')
@cross_origin()
def messages():
    youtube = get_youtube_authenticated_service()

    live_chat_id = request.args.get('liveChatId')

    response = youtube.liveChatMessages().list(
        liveChatId=live_chat_id,
        part='id,snippet,authorDetails'
    ).execute()

    chat = {
        'pollingInterval': response.get('pollingIntervalMillis', 1000),
        'items': []
    }

    for message in response.get('items', []):
        chat['items'].append({
            'author':  message['authorDetails']['displayName'],
            'message': {
                'text': message['snippet']['displayMessage'],
                'publishedAt': message['snippet']['publishedAt']
            }
        })


    return flask.jsonify(chat)

@app.route('/search')
@cross_origin()
def search():
    youtube = get_youtube_authenticated_service()

    text = request.args.get('text')

    livestreams_request = youtube.search().list(
        part='snippet',
        eventType='live',
        maxResults=25,
        q=text,
        type='video'
    )

    livestreams_list = []

    while livestreams_request:
        list_streams_response = livestreams_request.execute()

        for stream in list_streams_response.get('items', []):
            snippet = stream['snippet']
            medium_thumb = snippet['thumbnails']['medium']

            livestreams_list.append({
                'title': snippet['title'],
                'description': snippet['description'],
                'thumb': {
                    'url': medium_thumb['url'],
                    'w': medium_thumb['width'],
                    'h': medium_thumb['height']
                },
                'videoId': stream['id']['videoId'],
                'channelId': snippet['channelId']
            })

        livestreams_request = youtube.liveStreams().list_next(
            livestreams_request, list_streams_response)

    return flask.jsonify(livestreams_list)

def credentials_to_dict(credentials):
    return {
        'token': credentials.token,
        'refresh_token': credentials.refresh_token,
        'token_uri': credentials.token_uri,
        'client_id': credentials.client_id,
        'client_secret': credentials.client_secret,
        'scopes': credentials.scopes
    }

def get_credentials():
    credentials = google.oauth2.credentials.Credentials(
        **flask.session['credentials'])

    flask.session['credentials'] = credentials_to_dict(credentials)

    return credentials

def get_youtube_authenticated_service():
    if 'credentials' not in flask.session:
        return flask.redirect('register')

    credentials = get_credentials()

    return googleapiclient.discovery.build(
        API_SERVICE_NAME, API_VERSION, credentials=credentials)

def get_plus_authenticated_service():
    print('credentials' in flask.session)

    if 'credentials' not in flask.session:
        return flask.redirect('register')

    credentials = get_credentials()

    return googleapiclient.discovery.build(
        'plus', 'v1', credentials=credentials)

def get_flow():
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE,
        scopes=SCOPES)

    flow.redirect_uri = 'https://p.streaming.mvictorov.com/api/oauth2/callback'

    return flow
