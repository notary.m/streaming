from app import app
from app.controllers.auth import get_plus_authenticated_service
from app.controllers.auth import get_flow
from app.controllers.auth import get_credentials

from googleapiclient.http import build_http

from flask_cors import cross_origin

import os
import flask
import requests

@app.route('/user')
@cross_origin()
def get_user():
    service = get_plus_authenticated_service()
    userinfo = service.people().get(userId='me').execute()

    print(userinfo)

    return flask.jsonify(userinfo)

