#!bin/python3
from app import app
app.run(ssl_context=('/user/local/etc/ssl/certs/nginx-selfsigned.crt', '/usr/local/etc/ssl/private/nginx-selfsigned.key'))
